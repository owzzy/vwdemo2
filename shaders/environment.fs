#version 450 core

layout(binding = 0) uniform sampler2D ColTexture;
layout(binding = 1) uniform sampler2D StencilTexture;
layout(binding = 2) uniform sampler2D FlatTexture;
layout(binding = 3) uniform samplerCube SkyboxTexture;

in float Intensity;
in vec2 TexCoord;
in vec3 rv;

out vec4 outColour; // Color that will be used for the fragment

//////////////////////////////////////////////////////////////////
// main()
//////////////////////////////////////////////////////////////////
void main()
{
    outColour = texture(ColTexture, TexCoord) * Intensity;

    vec4 reflectedColour = texture(SkyboxTexture, rv); 
    outColour = mix (outColour, reflectedColour, 0.6);

    //vec4 colour = texture(SkyboxTexture, rv.xyz); 

    //FragColor = vec4(vec3(colour.xyz * Intensity), 1.0);
}